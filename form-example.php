<?php 

$pageTitle = "Our Services";
$breadcrumbs = array('Home', 'Some page');

$inputs = array(
	"color" => "color",
	"date" => "date",
	"datetime-local" => "datetime-local",
	"email" => "email",
	"file" => "file",
	/*"hidden" => "hidden",*/
	/*"image" => "image",*/
	"month" => "month",
	"number" => "number",
	"password" => "password",
	"range" => "range",
	"search" => "search",
	"tel" => "tel",
	"text" => "text",
	"time" => "time",
	"url" => "url",
	"week" => "week",
);

$buttonInputs = array(
	"reset" => "reset",
  "submit" => "submit",
);

$choiceInputs = array(
	"checkbox" => "checkbox",
  "radio" => "radio",
);

 ?>
<!doctype html>
<html lang="en" data-ng-app="app">
  <div class="outer-wrap">
    <div class="wrap">
      <div class="content-wrap">
        <div class="container-fluid pad-top-40 pad-bottom-40">
          <h1>Example form types</h1>

          <form class="responsive-form">

            <h2>Basic Input Types</h2>
            
            <?php foreach ($inputs as $i) { ?>

              <h3><?php print ucfirst($i) ?> Input</h3>

              <div class="field-group">
                <label for="<?php print $i ?>-input"><?php print ucfirst($i) ?> Input</label>
                <input type="<?php print $i ?>" id="<?php print $i ?>-input" name="<?php print $i ?>-input" />
              </div>

              <pre>
                &lt;div class="field-group"&gt;
                  &lt;label for="<?php print $i ?>-input"&gt;<?php print ucfirst($i) ?> Input&lt;/label&gt;
                  &lt;input type="<?php print $i ?>" id="<?php print $i ?>-input" name="<?php print $i ?>-input" /&gt;
                &lt;/div&gt;
              </pre>

              <div class="field-group inline">
                <label for="<?php print $i ?>-input-inline">Inline <?php print ucfirst($i) ?> Input</label>
                <input type="<?php print $i ?>" id="<?php print $i ?>-input-inline" name="<?php print $i ?>-input-inline" />
              </div>

              <pre>
                &lt;div class="field-group inline"&gt;
                  &lt;label for="<?php print $i ?>-input-inline"&gt;Inline <?php print ucfirst($i) ?> Input&lt;/label&gt;
                  &lt;input type="<?php print $i ?>" id="<?php print $i ?>-input-inline" name="<?php print $i ?>-input-inline" /&gt;
                &lt;/div&gt;
              </pre>

              <div class="field-group">
                <label for="<?php print $i ?>-input">Disabled <?php print ucfirst($i) ?> Input</label>
                <input type="<?php print $i ?>" id="<?php print $i ?>-input" name="<?php print $i ?>-input" disabled="" />
              </div>

              <pre>
                &lt;div class="field-group"&gt;
                  &lt;label for="<?php print $i ?>-input"&gt;Disabled <?php print ucfirst($i) ?> Input&lt;/label&gt;
                  &lt;input type="<?php print $i ?>" id="<?php print $i ?>-input" name="<?php print $i ?>-input" disabled="" /&gt;
                &lt;/div&gt;
              </pre>

              <hr />

              <?php	} ?>

            <h2>Button Inputs</h2>

            <?php foreach ($buttonInputs as $i) { ?>

              <h3>
                <?php print ucfirst($i) ?> Input
              </h3>

              <div class="field-group">
                <label for=""
                  <?php print $i ?>-input"><?php print ucfirst($i) ?> Input
                </label>
                <input type=""<?php print $i ?>" id="<?php print $i ?>-input" name="<?php print $i ?>-input" value="<?php print $i ?>" />
              </div>

              <pre>
                &lt;div class="field-group"&gt;
                  &lt;label for="<?php print $i ?>-input"&gt;<?php print ucfirst($i) ?> Input&lt;/label&gt;
                  &lt;input type="<?php print $i ?>" id="<?php print $i ?>-input" name="<?php print $i ?>-input" value="<?php print $i ?>" /&gt;
                &lt;/div&gt;
              </pre>

              <div class="field-group inline">
                <label for=""
                  <?php print $i ?>-input-inline">Inline <?php print ucfirst($i) ?> Input
                </label>
                <input type=""<?php print $i ?>" id="<?php print $i ?>-input-inline" name="<?php print $i ?>-input-inline" value="<?php print $i ?>" />
                </div>

              <pre>
                &lt;div class="field-group inline"&gt;
                  &lt;label for="<?php print $i ?>-input-inline"&gt;Inline <?php print ucfirst($i) ?> Input&lt;/label&gt;
                  &lt;input type="<?php print $i ?>" id="<?php print $i ?>-input-inline" name="<?php print $i ?>-input-inline" value="<?php print $i ?>" /&gt;
                &lt;/div&gt;
              </pre>

              <div class="field-group">
                <label for=""
                  <?php print $i ?>-input">Disabled <?php print ucfirst($i) ?> Input
                </label>
                <input type=""<?php print $i ?>" id="<?php print $i ?>-input" name="<?php print $i ?>-input" value="<?php print $i ?>" disabled="" />
                </div>

              <pre>
                &lt;div class="field-group"&gt;
                  &lt;label for="<?php print $i ?>-input"&gt;Disabled <?php print ucfirst($i) ?> Input&lt;/label&gt;
                  &lt;input type="<?php print $i ?>" id="<?php print $i ?>-input" name="<?php print $i ?>-input" value="<?php print $i ?>" disabled="" /&gt;
                &lt;/div&gt;
              </pre>

              <hr />

            <?php	}?>

            <h2>Choice Inputs</h2>
            
            <?php foreach ($choiceInputs as $i) { ?>

              <h3><?php print ucfirst($i) ?> Input</h3>

              <div class="field-group">
                <input type="<?php print $i ?>" id="<?php print $i ?>-input" name="<?php print $i ?>-input" />
                <label for="<?php print $i ?>-input"><?php print ucfirst($i) ?> Input</label>
              </div>

              <pre>
                &lt;div class="field-group"&gt;
                  &lt;input type="<?php print $i ?>" id="<?php print $i ?>-input" name="<?php print $i ?>-input" /&gt;
                  &lt;label for="<?php print $i ?>-input"&gt;<?php print ucfirst($i) ?> Input&lt;/label&gt;
                &lt;/div&gt;
              </pre>

              <div class="field-group inline">
                <input type="<?php print $i ?>" id="<?php print $i ?>-input-inline" name="<?php print $i ?>-input-inline" />
                <label for="<?php print $i ?>-input-inline">Inline <?php print ucfirst($i) ?> Input</label>
             </div>

              <pre>
                &lt;div class="field-group inline"&gt;
                  &lt;input type="<?php print $i ?>" id="<?php print $i ?>-input-inline" name="<?php print $i ?>-input-inline" /&gt;
                  &lt;label for="<?php print $i ?>-input-inline"&gt;Inline <?php print ucfirst($i) ?> Input&lt;/label&gt;
                &lt;/div&gt;
              </pre>

              <div class="field-group">
                <input type="<?php print $i ?>" id="<?php print $i ?>-input" name="<?php print $i ?>-input" disabled="" />
                <label for="<?php print $i ?>-input">Disabled <?php print ucfirst($i) ?> Input</label>
              </div>

              <pre>
                &lt;div class="field-group"&gt;
                  &lt;input type="<?php print $i ?>" id="<?php print $i ?>-input" name="<?php print $i ?>-input" disabled="" /&gt;
                  &lt;label for="<?php print $i ?>-input"&gt;Disabled <?php print ucfirst($i) ?> Input&lt;/label&gt;
                &lt;/div&gt;
              </pre>

              <hr />

              <?php	} ?>
            
          </form>
          
          
        </div>
      </div>
      <?php include '../inc/footer.php'; ?>
    </div>
  </div>  

</body>
</html>
