jamie-scott.co.uk - About this site and code
============================================

This site /code is a working, living experiment and exercise in creating a website from technologies used day to day at work. The site will contain code and concepts from but not exclusively...

* HTML5
* CSS3
* Sass
* Flexbox
* SMACSS
* jQuery
* SVG Images
* Cloudinary cdn for images, videos and files

This document will expand and link to a *living style guide* as it grows.