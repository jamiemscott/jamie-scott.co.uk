<!doctype html>
<html class="no-js" lang="">
  <head>
      <?php /* General Stuff */ ?>
      <meta charset="utf-8">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <title></title>
      <meta name="description" content="">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      
      <?php /* Favicons and App tiles */ ?>
      <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
      <link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
      <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
      <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
      <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
      <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
      <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
      <link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
      <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
      <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
      <link rel="icon" type="image/png" href="/android-chrome-192x192.png" sizes="192x192">
      <link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
      <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
      <link rel="manifest" href="/manifest.json">
      <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
      <meta name="msapplication-TileColor" content="#da532c">
      <meta name="msapplication-TileImage" content="/mstile-144x144.png">
      <meta name="theme-color" content="#000001">

      <?php /* Fonts */ ?>
      <link href='https://fonts.googleapis.com/css?family=Hind:400,300,500,600,700' rel='stylesheet' type='text/css'>
      <?php /*font-family: 'Hind', sans-serif;*/ ?>
      <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600italic,600,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
      <?php /*font-family: 'Open Sans', sans-serif;*/ ?>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
      
      <?php /* CSS */ ?>
      <link rel="stylesheet" href="assets/css/main.css">
      
      <?php /* Modernizr */ ?>
      <script src="assets/js/vendor/modernizr-2.8.3.min.js"></script>
  </head>
  <body>
      <!--[if lt IE 8]>
          <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
      <![endif]-->

      <div id="site-wrapper">
        
        <!-- BOF Header  -->
        <header id="home">
        
          <nav id="primary-navigation">
            
            <input type="checkbox" name="navigation-toggle" id="navigation-toggle" />
            <label for="navigation-toggle"></label>

            <ul>
              <li class="current"><a class="smoothscroll" href="#home">Home</a></li>
              <li><a class="smoothscroll" href="#about">About</a></li>
              <li><a class="smoothscroll" href="#resume">Resumé</a></li>
              <li><a class="smoothscroll" href="#portfolio">Portfolio</a></li>
              <li><a class="smoothscroll" href="#testimonials">Testimonials</a></li>
              <li><a class="smoothscroll" href="#contact">Contact</a></li>
            </ul> <!-- end #nav -->
          
          </nav> <!-- end #nav-wrap -->
          
          <div class="hero container">
            <h1>I'm Jamie Scott.</h1>
            <p>I'm a UK based <em>front end developer</em> and <em>web designer</em> specialising in clean, semantic accessible websites. When I'm not coding websites I can be found elbows deep in classic <em>Volkswagens</em> and <em>Minis</em> or even relaxing and <em>painting wargaming minatures</em>. Let's <a class="smoothscroll" href="#about">start scrolling</a> and learn more <a class="smoothscroll" href="#about">about me</a>.</p>
            <?php include('assets/includes/inc.social-links.php'); ?> 
            
            <a class="scroll-arrow" href="#about">Scroll</a>
          </div>
          
        </header>
        <!-- EOF Header End -->
        
        <!-- BOF About -->
        <section id="about" class="container dark-grey-background">
        
          <img class="profile-pic"  src="images/profilepic.jpg" alt="" />
          
          <h2>About Me</h2>
          
          <p>Successful web design professional with diverse and extensive experience focusing on semantic, standards compliant, search engine friendly site design. Proven achievements in agile based time managed projects in large team based projects to one on one client focused bespoke web sites. Considered an inavative team player with a reputation for integrity and quality. Team oriented with excellent interpersonal communication skills.</p>
          
          <p>Specialities: Adobe Creative Suite, Content Management Systems including Wordpress, Drupal and Joomla, XHTML, HTML5, XML, CSS, LESS, Sass, UX and UI, Wireframing, Site Architecture Planning and Implementing, HTML Email Design and Production, PHP Editing and Adaptation, jQuery Implementation, Agile Based Project Planning</p>
          
          <p>Future Goals: Looking into expanding my skill set into javascript frameworks such as AngularJS and task runners such as Grunt and Gulp for more efficient site development. Upgrade this little website to use more modern techniques such as Flexbox and Sass/SMACSS and get a blog underway probably converting things to use Drupal.</p>
          
          <h2>Contact Details</h2>
          
          <div id="hcard-Jamie-Scott" class="vcard">
            <span class="fn">Jamie Scott</span>
            
            <div class="adr">
              <span class="street-address">3 Tattersall Chase</span>
              <span class="locality">Chelmsford</span>, 
              <span class="region">Essex</span>, 
            </div>
            <div class="tel">07885 548 018</div>
            <a class="email" href='mailto&#58;j&#37;61%&#54;&#68;%69e&#64;%6A&#97;m%69&#101;&#37;2Dsc&#111;t&#116;&#46;c&#111;&#46;%75%6B'>jamie&#64;jamie-sc&#111;t&#116;&#46;c&#111;&#46;uk</a>
          </div>

          <a href="http://res.cloudinary.com/jamie-scott-co-uk/raw/upload/v1461081349/JamieScott-Resume-2016.pdf" class="button"><i class="fa fa-download"></i>Download Resume</a>

        </section> 
        <!-- EOF About -->

      </div> <!-- EOF #site-wrapper -->
    
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery-1.11.3.min.js"><\/script>');</script>
      <script src="assets/js/plugins.js"></script>
      <script src="assets/js/main.js"></script>

      <!-- Google Analytics -->
      <script>
       (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
       (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
       m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
       })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
     
       ga('create', 'UA-44399950-1', 'auto');
       ga('send', 'pageview');
     </script>
     
  </body>
</html>
