<?php

use Contentful\Delivery\Client;

require_once __DIR__ . '/vendor/autoload.php';

$spaceID = '8tsehjlyjn2w';
$accessToken = '6f124cbcb26a4c0a6ebcc22af2eb34900a8ad06b35794d1f02d8ff0965b9d0e6';

$client = new Client($accessToken, $spaceID);

$entries = $client->getEntries();

if (count($entries) === 0) {
    echo "Ups, you got no entries in your space. How about creating some?<br />";
}
else {
    echo "You have entries with these IDs:<br />";
    foreach ($entries as $entry) {
        echo $entry->getId() . "<br />";
    }
}
